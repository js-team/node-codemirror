const {lezer} = require("@lezer/generator/rollup")

exports.options = {
  expandLink: anchor => "https://codemirror.net/6/docs/ref/#" + anchor,
  pureTopCalls: true,
  outputPlugin: () => lezer()
}
